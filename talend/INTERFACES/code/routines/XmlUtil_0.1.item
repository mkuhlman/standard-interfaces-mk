package routines;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.StringReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.w3c.dom.Document;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import org.dom4j.io.DOMReader;


/*
 * user specification: the function's comment should contain keys as follows: 1. write about the function's comment.but
 * it must be before the "{talendTypes}" key.
 * 
 * 2. {talendTypes} 's value must be talend Type, it is required . its value should be one of: String, char | Character,
 * long | Long, int | Integer, boolean | Boolean, byte | Byte, Date, double | Double, float | Float, Object, short |
 * Short
 * 
 * 3. {Category} define a category for the Function. it is required. its value is user-defined .
 * 
 * 4. {param} 's format is: {param} <type>[(<default value or closed list values>)] <name>[ : <comment>]
 * 
 * <type> 's value should be one of: string, int, list, double, object, boolean, long, char, date. <name>'s value is the
 * Function's parameter name. the {param} is optional. so if you the Function without the parameters. the {param} don't
 * added. you can have many parameters for the Function.
 * 
 * 5. {example} gives a example for the Function. it is optional.
 */
public class XmlUtil {
	
	/**
	 * Returns the source XML, with the specified namespace/QName combinations added to the XML and all nodes mapped to the specified default QName
	 * 
	 * {talendTypes} Object
	 * {Category} User Defined
	 * {param} Document source: the source XML to be evaluated
	 * {param} Map<String, String> namespaces: map from namespace to the QName to use for that namespace
	 * {param} String defaultQName: the QName to apply as the default on all of the nodes in the XML
	 * 
	 * @param source the source XML to be evaluated
	 * @param namespaces map from namespace to the QName to use for that namespace
	 * @param defaultQName the QName to apply as the default on all of the nodes in the XML
	 * @return XML with namespace information added
	 */
	public static routines.system.Document addXmlNamespaces(routines.system.Document source, java.util.Map<String, String> namespaces, String defaultQName)
			throws Exception {
		final String rootNodeName = source.getDocument().getRootElement().getName();
		final String defaultNamespace = namespaces.get(defaultQName);
		
		final StringBuilder xslt = new StringBuilder();
		xslt.append("<xsl:template match=\"").append(rootNodeName).append("\">")
				.append("<").append(defaultQName).append(":").append(rootNodeName);
		
		namespaces.forEach((qName, namespace) -> xslt.append(" xmlns:").append(qName).append("=\"").append(namespace).append("\""));
		
		xslt.append(">")
				.append("<xsl:apply-templates/>")
				.append("</").append(defaultQName).append(":").append(rootNodeName).append(">")
				.append("</xsl:template>")
				.append("<xsl:template match=\"").append(rootNodeName).append("//*\">")
				.append("<xsl:element name=\"").append(defaultQName).append(":{name()}\" namespace=\"").append(defaultNamespace).append("\">")
				.append("<xsl:apply-templates/>")
				.append("</xsl:element>")
				.append("</xsl:template>");
		
		
		return applyXslt(source, getXslt(xslt.toString(), namespaces));
	}
	
	/**
	 * Replaces the namespace prefix of the specified node with the new value
	 * 
	 * {talendTypes} Object
	 * {Category} User Defined
	 * {param} Document source: the source XML to be evaluated
	 * {param} Map<String, String> namespaces: map from namespace to the QName to use for that namespace
	 * {param} String oldQName: the existing QName for the namespace to be replaced on the element
	 * {param} String newQName: the new QName for the namespace to apply to the element
	 * {param} String elementName: the name of the element to be modified in the XML
	 * 
	 * @param source the source XML to be evaluated
	 * @param namespaces map from namespace to the QName to use for that namespace 
	 * @param oldQName the existing namespace for the element
	 * @param newQName the new namespace that should be used for the element
	 * @param elementName the name of the element that should be modified
	 * @return XML with namespace prefixes modified
	 */
	public static routines.system.Document changeNamespaceForElement(routines.system.Document source, java.util.Map<String, String> namespaces,
			 														 String oldQName, String newQName, String elementName) throws Exception {
		final String xslt = "<xsl:template match=\"@*|node()\">" +
				"<xsl:copy>" +
				"<xsl:apply-templates select=\"@*|node()\"/>" +
				"</xsl:copy>" +
				"</xsl:template>" +
				"<xsl:template match=\"" + oldQName + ":" + elementName + "\">" +
				"<" + newQName + ":" + elementName + ">" +
				"<xsl:apply-templates select=\"@*|node()\"/>" +
				"</" + newQName + ":" + elementName + ">" +
				"</xsl:template>";
		
		return applyXslt(source, getXslt(xslt.toString(), namespaces));
	}
	
	/**
	 * Returns the source XML, with all namespace information removed.  This allows for the XML to more cleanly be turned into JSON format
	 * 
	 * {talendTypes} Object
	 * {Category} User Defined
	 * {param} Document source: the source XML to be evaluated
	 * 
	 * @param source the source XML to be evaluated
	 * @return XML with all namespace information removed
	 */
	public static routines.system.Document removeXmlNamespaces(routines.system.Document source) throws Exception {
		return applyXslt(source, getXslt(NAMESPACE_REMOVAL_XSLT));
	}
	
	/**
	 * Returns the source XML with all occurrences of the specified attribute removed.  This allows for the XML to more cleanly be turned into JSON format
	 * 
	 * {talendTypes} Object
	 * {Category} User Defined
	 * {param} Document source: the source XML to be evaluated
	 * {param} String attributeName: the name of the attribute to be removed
	 * 
	 * @param source the source XML to be evaluated
	 * @param attributeName the name of the attribute to be removed
	 * @return XML with the specified attribute removed
	 */
	public static routines.system.Document removeXmlAttribute(routines.system.Document source, String attributeName) throws Exception {
		final String attributeRemovalXslt = getXslt(ATTRIBUTE_REMOVAL_XSLT.replace(PLACEHOLDER, attributeName));
		return applyXslt(source, attributeRemovalXslt);
	}
	
	/**
	 * Applies the specified XSL transformation to the provided XML
	 * 
	 * @param source XML to be transformed
	 * @param xslt transformation to apply to the XML
	 * @return transformed XML
	 * @throws Exception if there is an error transforming the XML
	 */
	private static routines.system.Document applyXslt(routines.system.Document source, String xslt) throws Exception {
		final TransformerFactory factory = TransformerFactory.newInstance();
		final Source transformation = new StreamSource(new StringReader(xslt));
		final Transformer transformer = factory.newTransformer(transformation);
		
		final Source xmlToTransform = new StreamSource(new StringReader(source.toString()));
		final ByteArrayOutputStream transformedXml = new ByteArrayOutputStream();
		transformer.transform(xmlToTransform, new StreamResult(transformedXml));
		
		final String transformedString = transformedXml.toString();
		
		final routines.system.Document output = new routines.system.Document();
		output.setDocument(new DOMReader().read(convertStringToDocument(transformedString)));
		return output;
	}
	
	/**
	 * Returns a DOM document containing the content of the specified string
	 * 
	 * @param source string containing the XML content to be processed
	 * @return DOM document containing the XML content
	 * @throws ParserConfigurationException if there is an error defining the XML parser
	 * @throws IOException if there is an error reading the source XML data
	 * @throws SAXException if the source data is not correctly formatted as XML
	 */
	private static Document convertStringToDocument(String source) throws ParserConfigurationException, IOException, SAXException {
		final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		final DocumentBuilder builder = factory.newDocumentBuilder();
		return builder.parse(new InputSource(new StringReader(source)));
	}
	
	/**
	 * Constructs a properly-formatted XSLT containing the provided XSLT content
	 *  
	 * @param xsltContent XSLT content to be included in the XSLT
	 * @return formatted XSLT
	 */
	private static String getXslt(String xsltContent) {
		return XSL_STYLESHEET_HEADER.replace(PLACEHOLDER, "") + xsltContent + XSL_STYLESHEET_FOOTER;
	}
	
	/**
	 * Constructs a properly-formatted XSLT containing the provided XSLT content and defining the specified namespace
	 * 
	 * @param xsltContent XSLT content to be included in the XSLT
	 * @param namespace namespace to define in the stylesheet header
	 * @param qName QName to use for the namespace
	 * @return formatted XSLT
	 */
	private static String getXslt(String xsltContent, java.util.Map<String, String> namespaces) {
		final StringBuilder namespaceInfo = new StringBuilder();
		namespaces.forEach((qName, namespace) -> {
			namespaceInfo.append(" xmlns:").append(qName).append("=\"").append(namespace).append("\"");
		});
		
		return XSL_STYLESHEET_HEADER.replace(PLACEHOLDER, namespaceInfo.toString()) + xsltContent + XSL_STYLESHEET_FOOTER;
	}
	

	/** String for use in XSLT definitions as a placeholder */
	private static final String PLACEHOLDER = "__PLACEHOLDER__";
	
	private static final String XSL_STYLESHEET_HEADER = "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\"" + PLACEHOLDER + ">";
	private static final String XSL_STYLESHEET_FOOTER = "</xsl:stylesheet>";
	
	/** XSL transformation to apply to remove namespaces from XML */
	private static final String NAMESPACE_REMOVAL_XSLT = "<xsl:template match=\"*\">" +
	                                                     "    <xsl:element name=\"{local-name(.)}\">" +
	                                                     "        <xsl:apply-templates select=\"@* | node()\" />" +
	                                                     "    </xsl:element>" +
	                                                     "</xsl:template>" +
	                                                     "<xsl:template match=\"@*\">" +
	                                                     "    <xsl:attribute name=\"{local-name(.)}\">" +
	                                                     "        <xsl:value-of select=\".\" />" +
	                                                     "    </xsl:attribute>" +
	                                                     "</xsl:template>";
	
	/** XSL transformation to apply to remove a specific attribute from XML */
	private static final String ATTRIBUTE_REMOVAL_XSLT = "<xsl:template match=\"@" + PLACEHOLDER + "\" />" +
														 "<xsl:template match=\"@* | node()\">" +
														 "    <xsl:copy>" +
														 "        <xsl:apply-templates select=\"@* | node()\" />" +
														 "    </xsl:copy>" +
														 "</xsl:template>";
}
