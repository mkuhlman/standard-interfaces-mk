package routines;

/*
 * user specification: the function's comment should contain keys as follows: 1. write about the function's comment.but
 * it must be before the "{talendTypes}" key.
 * 
 * 2. {talendTypes} 's value must be talend Type, it is required . its value should be one of: String, char | Character,
 * long | Long, int | Integer, boolean | Boolean, byte | Byte, Date, double | Double, float | Float, Object, short |
 * Short
 * 
 * 3. {Category} define a category for the Function. it is required. its value is user-defined .
 * 
 * 4. {param} 's format is: {param} <type>[(<default value or closed list values>)] <name>[ : <comment>]
 * 
 * <type> 's value should be one of: string, int, list, double, object, boolean, long, char, date. <name>'s value is the
 * Function's parameter name. the {param} is optional. so if you the Function without the parameters. the {param} don't
 * added. you can have many parameters for the Function.
 * 
 * 5. {example} gives a example for the Function. it is optional.
 */
public class Util {

    /**
     * isEmptyString: returns true when string is empty
     * 
     * 
     * {talendTypes} String
     * 
     * {Category} User Defined
     * 
     * {param} string("world") input: The string to check
     * 
     * {example} isEmptyString("world") # false
     */
    public static boolean isEmptyString(String s) {
        return s == null || s.isEmpty();
    }
    
    /**
     * concatNonEmptyString: returns the non empty strings concatenated together
     * 
     * 
     * {talendTypes} String
     * 
     * {Category} User Defined
     * 
     * {param} string("; ") delimiter: what to separate the strings with
     * 
     * {param} string("world") arr: comma separated strings or array of strings to concatenate
     * 
     * {example} concatNonEmptyString("; ", "world", "hello") # world; hello
     */
    public static String concatNonEmptyString(String delimiter, String... arr) {
    	StringBuffer buf = new StringBuffer();
    	for (String s: arr) {
    		if (!isEmptyString(s)) {
    			if (!buf.toString().isEmpty()) {
    				buf.append(delimiter);
    			}
    			buf.append(s);
    		}
    	}
    	return buf.toString();
    }
}
